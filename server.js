require('newrelic');
var env = require('wnd-env');
var errorLogger = require("wnd-logger");
var requestLogger = require('morgan');
var express = require('express');
var compression = require('compression');
var swagger = require("wnd-swagger-server");

errorLogger.configure(!env.isDev, env.get('LOGGER_SERVER_TOKEN'), env.get('NODE_ENV'));

var router = require('./router');
require('./entities');

var app = express();
app.disable('x-powered-by');

// log every request / response
app.use(requestLogger(env.get('LOG_FORMAT', 'combined')));

// gzip/deflate outgoing responses
app.use(compression());

router.configure(app);

app.listen(env.get('PORT'), function() {
  console.log('Express server listening on port ' + env.get('PORT'));
});
