FROM wonderlic/nodejs:0.10.36
MAINTAINER Wonderlic DevOps <DevOps@wonderlic.com>

COPY entities /app/entities
COPY node_modules /app/node_modules
COPY router /app/router
COPY database.js /app/database.js
COPY env-rules.js /app/env-rules.js
COPY newrelic.js /app/newrelic.js
COPY server.js /app/server.js

RUN ln -s /usr/bin/node /app/api-template

CMD ["/app/api-template", "/app/server.js"]
