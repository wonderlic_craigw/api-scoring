var env = require('wnd-env');
var DBFactory = require('wnd-database');

var dbtype = 'mongo';
var dbOptions = {mongoUri: env.get('MONGO_TEST_URL')};
var collections = [
  'things'
];

module.exports = DBFactory.getDatabase(dbtype, dbOptions, collections);
