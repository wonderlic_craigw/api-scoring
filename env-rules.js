module.exports = {
  NODE_ENV: {choices: ['production', 'test', 'development']},
  LOG_FORMAT: {},
  LOGGER_SERVER_TOKEN: {},
  TEST_REPORTER: {},
  PORT: {parse: 'toNumber'},
  API_URL: {},
  MONGO_URL: {},
  MONGO_TEST_URL: {},
  NEW_RELIC_ENABLED: {parse: 'toBoolean'},
  NEW_RELIC_LICENSE_KEY: {},
  NEW_RELIC_APP_NAME: {}
};
