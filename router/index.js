var env = require('wnd-env');
var express = require('express');
var errorLogger = require("wnd-logger");
var swagger = require('wnd-swagger-server');

function Router() {

  this.configure = function(app) {

    var swaggerApp = express();
    app.use("/scoring/v1", swaggerApp);
    swagger.setAppHandler(swaggerApp);

    var thingMethods = require('../entities/thing/methods');
    swagger.post("/things", thingMethods.createThing);
    swagger.get("/things/{thingId}", thingMethods.getThingById);

    var apiMethods = require('../entities/api/methods');
    swagger.post("/api/PCI", apiMethods.scorePCI);

    // Error logging should be the last thing wired up before swagger.finalize...
    swaggerApp.use(errorLogger.errorLogger);

    // This is required for the api-docs to work
    swagger.finalize(env.get('API_URL') + '/scoring/v1', '1.0');
  };
}

module.exports = new Router();
