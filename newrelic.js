var env = require('wnd-env');

if (env.get('NEW_RELIC_ENABLED', {required: true})) {
  env.require(['NEW_RELIC_APP_NAME', 'NEW_RELIC_LICENSE_KEY']);
}

/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  // reads from NEW_RELIC_APP_NAME env variable
  //app_name : ['My Application'],
  /**
   * Your New Relic license key.
   */
  // reads from NEW_RELIC_LICENSE_KEY env variable
  //license_key : 'license key here',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level: 'info'
  }
};
