var createCustomError = require('custom-error-generator');

module.exports.InvalidParamsInputError = createCustomError('InvalidParamsInputError');
module.exports.InvalidObjectInputError = createCustomError('InvalidObjectInputError');
module.exports.NotFoundInputError = createCustomError('NotFoundInputError');