var swagger = require('wnd-swagger-server');

var ApiService = require('./service');

function ApiMethods() {

  this.scorePCI = {
    spec: {
      nickname: "PCI_ScorePCI",
      summary: "",
      type: "PCI_Response",
      parameters: [
        swagger.bodyParam("body", "", "PCI_Request")
      ],
      responseMessages: [
        swagger.responses.ok,
        new swagger.InvalidError('PCI_Request'),
        new swagger.ServerError()
      ]
    },
    action: function(req, res, next) {
      var service = new ApiService(req.user);
      service.scorePCI(req.body)
        .then(function(result) {
          res.json(result);
        })
        .catch(function(err) {
          switch (err.name) {
            case 'InvalidObjectInputError':
              return next(new swagger.InvalidError('PCI_Request', err));
            default:
              return next(new swagger.ServerError(err));
          }
        });
    }
  };

}

module.exports = new ApiMethods();
