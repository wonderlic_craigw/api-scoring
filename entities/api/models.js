function ApiModels() {

  this.PCI_Request = {
    type: "object",
    id: "PCI_Request",
    required: ["scoringParameters", "responses"],
    properties: {
      scoringParameters: {"$ref": "PCI_ScoringParameters"},
      responses: {type: "array", items: {"$ref": "Response"}}
    },
    subTypes: []
  };

  this.PCI_ScoringParameters = {
    type: "object",
    id: "PCI_ScoringParameters",
    required: ["assessmentPurpose"],
    properties: {"assessmentPurpose": {type: "string"}},
    subTypes: []
  };

  this.Response = {
    type: "object",
    id: "Response",
    required: ["question"],
    properties: {
      "response": {type: "string"},
      "question": {type: "integer", format: "int32"}
    },
    subTypes: []
  };

  this.PCI_Response = {
    type: "object",
    id: "PCI_Response",
    required: ["doer", "reward", "inspired", "value", "enjoyment", "relationships", "stay", "motivationPotential"],
    properties: {
      "doer": {type: "integer", format: "int32"},
      "reward": {type: "integer", format: "int32"},
      "inspired": {type: "integer", format: "int32"},
      "value": {type: "integer", format: "int32"},
      "enjoyment": {type: "integer", format: "int32"},
      "relationships": {type: "integer", format: "int32"},
      "stay": {type: "integer", format: "int32"},
      "motivationPotential": {type: "integer", format: "int32"}
    },
    subTypes: []
  };
};

module.exports = new ApiModels();
