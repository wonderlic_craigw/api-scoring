var _ = require('lodash');
var Q = require('q');
var swagger = require('wnd-swagger-server');

var errors = require('../errors.js');
var apiModels = require("./models");

function ApiService(user, config) {
  config = config || {};

  this.user = user;
  this.database = config.database || require('../../database');
}

ApiService.prototype.scorePCI = function(pciRequest) {
  var swaggerErrors = swagger.validate('PCI_Request', pciRequest, apiModels);
  if (swaggerErrors) {
    return Q.reject(new errors.InvalidObjectInputError(swaggerErrors));
  }

  // Do PCI Scoring

  return Q.resolve(pciRequest);
};

/* Example JSON format for the PCI scorer input
    {
    'scoringParameters': {
    'assessmentPurpose': 'SelectionOrPromotion'
    },
    'responses': [
    {'response': '3', 'question': 1},
    {'response': '1', 'question': 2},
    {'response': '3', 'question': 3},
    {'response': '5', 'question': 4},
    {'response': '3', 'question': 5},
    {'response': '2', 'question': 6},
    {'response': '4', 'question': 7},
    {'response': '5', 'question': 8},
    {'response': '5', 'question': 9},
    {'response': '1', 'question': 10}
    ... (for all 30)
    ]}
    */

module.exports = ApiService;
