var swagger = require('wnd-swagger-server');

var ThingService = require('./service');

function ThingMethods() {

  this.createThing = {
    spec: {
      nickname: "createThing",
      summary: "createThing",
      type: "thingId",
      parameters: [
        swagger.bodyParam("body", "thing to create", "newThing")
      ],
      responseMessages: [
        swagger.responses.ok,
        new swagger.InvalidError('thing'),
        new swagger.ServerError()
      ]
    },
    action: function(req, res, next) {
      var service = new ThingService(req.user);
      service.createThing(req.body)
        .then(function(thing) {
          res.json({id: thing.id});
        })
        .catch(function(err) {
          switch (err.name) {
            case 'InvalidObjectInputError':
              return next(new swagger.InvalidError('thing', err));
            default:
              return next(new swagger.ServerError(err));
          }
        });
    }
  };

  this.getThingById = {
    spec: {
      nickname: "getThingById",
      summary: "getThingById",
      type: "thing",
      parameters: [
        swagger.pathParam("thingId", "ID of thing", "string")
      ],
      responseMessages: [
        swagger.responses.ok,
        new swagger.NotFoundError('thing'),
        new swagger.ServerError()
      ]
    },
    action: function(req, res, next) {
      var service = new ThingService(req.user);
      service.getThingById(req.params.thingId)
        .then(function(thing) {
          if (!thing) { return next(new swagger.NotFoundError('thing')); }
          res.json(thing);
        })
        .catch(function(err) {
          switch (err.name) {
            default:
              return next(new swagger.ServerError(err));
          }
        });
    }
  };
}

module.exports = new ThingMethods();
