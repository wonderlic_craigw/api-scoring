var _ = require('lodash');
var Q = require('q');
var swagger = require('wnd-swagger-server');

var errors = require('../errors.js');
var thingModels = require("./models");

function ThingService(user, config) {
  config = config || {};

  this.user = user;
  this.database = config.database || require('../../database');
}

ThingService.prototype.createThing = function(newThing) {
  var swaggerErrors = swagger.validate('newThing', newThing, thingModels);
  if (swaggerErrors) {
    return Q.reject(new errors.InvalidObjectInputError(swaggerErrors));
  }

  if (newThing.id) {
    return Q.reject(new errors.InvalidObjectInputError("Thing already has id property"));
  }

  return this.database.getInstance()
    .then(function(db) {
      return db.things.insert(newThing);
    });
};

ThingService.prototype.getThingById = function(thingId) {
  return this.database.getInstance()
    .then(function(db) {
      return db.things.get({id: thingId});
    });
};

module.exports = ThingService;
