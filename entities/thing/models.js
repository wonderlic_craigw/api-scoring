function ThingModels() {

  this.thingId = {
    id: "thingId",
    required: ["id"],
    properties: {
      id: {type: "string"}
    }
  };

  this.newThing = {
    id: "newThing",
    required: ["name"],
    properties: {
      name: {type: "string"}
    }
  };

  this.thing = {
    id: "thing",
    required: ["id", "name"],
    properties: {
      id: {type: "string"},
      name: {type: "string"}
    }
  };
}

module.exports = new ThingModels();
