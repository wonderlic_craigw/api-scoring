var expect = require("chai").expect;

var ThingService = require('./service');

var service = new ThingService(
  {id: 1, account: {id: 1}}, // default user; can be overridden in tests if necessary
  {database: require('../../testFramework/test_database')}
);

describe('ThingService', function() {

  describe('.getThingById', function() {

    it('should return the thing', function() {
      return service.getThingById("1")
        .then(function(thing) {
          expect(thing).to.exist;
          expect(thing.id).to.exist;
          expect(thing.id).to.equal("1");
        });
    });

    it('should not return a thing if the id was not found', function() {
      return service.getThingById("NotValid")
        .then(function(thing) {
          expect(thing).to.not.exist;
        });
    });
  });

  describe('.createThing', function() {

    it('should create the thing', function() {
      return service.createThing({"name": "This is a name"})
        .then(function(thing) {
          expect(thing).to.exist;
          expect(thing.id).to.exist;
          return [thing, service.getThingById(thing.id)];
        })
        .spread(function(createThing, getThing) {
          expect(getThing).to.exist;
          expect(getThing.id).to.equal(createThing.id);
        });
    });

    it('should create the thing with extra fields that are not part of the model', function() {
      return service.createThing({
        "name": "This is a name",
        "what": "huh",
        "thisisntright": true
      })
        .then(function(thing) {
          expect(thing).to.exist;
          expect(thing.id).to.exist;
          return service.getThingById(thing.id);
        })
        .then(function(thing) {
          expect(thing.id).to.exist;
          expect(thing.name).to.exist;
          expect(thing.name).to.equal("This is a name");
          expect(thing.what).to.exist;
          expect(thing.what).to.equal("huh");
          expect(thing.thisisntright).to.exist;
          expect(thing.thisisntright).to.equal(true);
        });
    });

    it('should throw an error if a required field is missing', function() {
      return service.createThing({})
        .then(function(thing) {
          expect(thing).to.not.exist;
        })
        .catch(function(err) {
          expect(err).to.exist;
          expect(err.message).to.exist;
          expect(err.message).to.equal("name is required");
        });
    });
  });
});
