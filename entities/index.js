var swagger = require('wnd-swagger-server');
var entities = ['thing', 'api'];

entities.forEach(function(entity) {
  var models = require('./' + entity + '/models');
  swagger.addModels({models: models});
});
