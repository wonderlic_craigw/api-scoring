var env = require('wnd-env');
var gulp = require('gulp');
var apiTasks = require('wnd-gulp-api-tasks');

var database = require('./database');
var test_database = require('./testFramework/test_database');

function closeThings() {
  database.close();
  test_database.close();
}

var universalData = require('./data/universal');
var devData = [universalData, require('./data/manualTest')];
var testData = [universalData, require('./data/unitTest')];

gulp.task('seed_db', apiTasks.database.seedDatabase(database, null, null, universalData, true, closeThings));
gulp.task('seed_dev_db', apiTasks.database.seedDatabase(database, devData, devData, null, env.isDev, closeThings));
gulp.task('seed_test_db', apiTasks.database.seedDatabase(test_database, testData, testData, null, true, closeThings));

gulp.task('default', ['test'], function() { gulp.start(['serve', 'watch']); });
gulp.task('watch', function() {
  gulp.watch(['data/{universal,unitTest}/**/*.*', 'entities/**/*.*'], ['test']);
  gulp.watch('data/{universal,manualTest}/**/*.*', ['seed_dev_db']);
});
gulp.task('serve', ['seed_dev_db'], apiTasks.nodemon.serve({
  nodeArgs: env.isDev ? ['--debug=' + (env.get('PORT') + 50000)] : [],
  script: 'server.js',
  watch: [
    '{database,env-rules,server}.js',
    '{entities,router}/**/*.js',
    'data/{universal,manualTest}/**/*.*'
  ]
}));

gulp.task('test', ['seed_test_db'], apiTasks.testing.runTests('entities/**/*.test.js', {reporter: env.get('TEST_REPORTER', 'spec')}, closeThings));
gulp.task('coverage', ['seed_test_db'], apiTasks.testing.coverage(
  'entities/**/*.test.js',
  ['entities/**/*.js', '!**/*.test.js'], {
    dir: './test-coverage',
    reporters: ['lcov', 'json', 'text', 'text-summary'],
    reportOpts: {dir: './test-coverage'}
  }, closeThings
));
